# Sample GitLab Project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

Peter Wang [Contact with me in Microsoft Teams - deep link](https://teams.microsoft.com/l/chat/0/0?users=xin.wang@ubisoft.com&topicName=Knowledge%20Management%20GitLab%20Wiki&message=Hello%20there%2C)

TBD
